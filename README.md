# LabSentry.com Custom Client Starter Kit (React)

This project provides a starter kit that lets you access your LabSentry.com devices.  You need an active LabSentry.com 
account in order to use this project.  This example starter kit has been made with as few dependencies as possible in 
order to clearly show the minimal requirements fo building a LabSentry.com client.


## Components
 1. _React_ This example client uses the React javascript framework and is based on create-react-app
 1. _Auth0-Lock_ LabSentry.com's authentication system is powered by the same technologies behind Auth0
 1. _Apollo_ This library is used for accessing graphQL data - LabSentry's primary API.  

## installing the dependencies
Because the application is based on create-react-app, first install this globally, then install the package dependencies.

```bash
npm install -g create-react-app
npm install
```

## Connection to LabSentry.com
 1. By default this project connects to the LabSentry.com Acceptance Test environment.  This can be modified by editing the
 `src\routes.js` file and editing the uri on this line 
    ```
    const networkInterface = createNetworkInterface({uri: 'https://ssl-proxy.labsentry.com:36105/graphql'});
    ```
 
## Running the Starter Kit.
```sh
npm start
```
Then open your browser and point it to: `localhost:3000`  Login using your LabSentry.com account.


## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more info.


