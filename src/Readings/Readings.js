import React from 'react'
import PropTypes from 'prop-types'
import {graphql, gql} from 'react-apollo'
import Callback from './../Callback/Callback'
import './Readings.css'
import Moment from 'moment'
import {Button} from 'react-bootstrap';


class Readings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastUpdate: null,
      showExtended: false
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({lastUpdate: Date.now()})
  }

  static propTypes = {
    data: PropTypes.object,
  }

  convertType(uidString) {
    switch (uidString) {
      case 'ROOM_X_T':
        return "Temperature";
      case 'ROOM_X_H':
        return "Humidity";
      case 'ROOM_X_L.C':
        return "Light";

    }
  }

  convertQuality(quality) {
    switch (quality) {
      case 192:
        return "Good"
    }
  }

  convertUnits(type, value) {
    switch (type) {
      case "Temperature":
        return Math.round(value * 9 / 5 + 32) + "°F";
      case "Humidity" :
        return Math.round(value) + "%";
      case "Light" :
        return Math.round(value);
    }
  }

  formatTimestamp(timestamp) {
    if (timestamp === null) {
      return "never"
    }
    let ts = Moment.utc(timestamp);
    return ts.fromNow();
  }

  renderPointRow(point) {
    let pointType = this.convertType(point.pointConfigByPoint.uid);

    return (
      <tr key={point.point}>
        <td>{pointType}</td>
        <td>{this.convertUnits(pointType, point.value)}</td>
        <td>{this.formatTimestamp(point.deviceTime)}</td>
        {this.state.showExtended ? <td>{this.formatTimestamp(point.receivedTime)}</td> : null }
        {this.state.showExtended ? <td>{this.convertQuality(point.quality)}</td> : null}
        {this.state.showExtended ? <td>{point.point}</td> : null}
      </tr>
    )
  }

  render() {
    if (this.props.data.loading) {
      return (<Callback/>)
    }

    return (
      <div className='w-100 flex justify-center'>
        <div className='w-100' style={{maxWidth: 400}}>
          <table>
            <thead>
            <tr>
              <td>Type</td>
              <td className="value-column">Value</td>
              <td className="time-ago">Captured</td>
              {this.state.showExtended ? <td className="time-ago">Received</td> : null}
              {this.state.showExtended ? <td>Quality</td> : null }
              {this.state.showExtended ? <td>Id</td> : null}
              <td className="show-hide" onClick={() => this.setState({showExtended: !this.state.showExtended})}>
                {this.state.showExtended ? "-" : "+" }
              </td>
            </tr>
            </thead>
            <tbody>
            {this.props.data.allLivePoints.nodes.map((pointNode) => this.renderPointRow(pointNode))}
            </tbody>
          </table>
          <div className="refresh-block">
            <Button bsStyle="primary" className="btn-margin"
                    onClick={() => this.props.data.refetch() && this.setState({lastUpdate: Date.now()})}>
              Refresh
            </Button>
            <div className="last-loaded">
              Last Updated: {Moment(this.state.lastUpdate).format("h:mm:ss a MMM Do YYYY")}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const allLivePoints = gql`query allLivePoints {
  allLivePoints {
    nodes {
      point, value, deviceTime, receivedTime, quality, 
      pointConfigByPoint {
        uid
      }
    }
  }
}`

export default graphql(allLivePoints, {options: {fetchPolicy: 'network-only'}})(Readings)

