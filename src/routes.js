import React from 'react';
import {Redirect, Route, BrowserRouter} from 'react-router-dom';
import App from './App';
import Home from './Home/Home';
import Profile from './Profile/Profile';
import Callback from './Callback/Callback';
import Auth from './Auth/Auth';
import history from './history';
import Readings from './Readings/Readings';
import {ApolloClient, ApolloProvider, createNetworkInterface} from 'react-apollo';


const auth = new Auth();

const handleAuthentication = (nextState, replace) => {
  if (/access_token|id_token|error/.test(nextState.location.hash)) {
    auth.handleAuthentication(nextState.location.hash);
  }
}

const networkInterface = createNetworkInterface({uri: 'https://ssl-proxy.labsentry.com:36106/graphql'})

// use the auth0IdToken in localStorage for authorized requests
networkInterface.use([{
  applyMiddleware (req, next) {
    if (!req.options.headers) {
      req.options.headers = {}
    }

    // get the authentication token from local storage if it exists
    if (localStorage.getItem('access_token')) {
      req.options.headers.authorization = `Bearer ${localStorage.getItem('access_token')}`
    }
    next()
  },
}])

const client = new ApolloClient({networkInterface})

export const makeMainRoutes = () => {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter history={history} component={App}>
        <div>
          <Route path="/" render={(props) => <App auth={auth} {...props} />}/>
          <Route exact path="/" render={(props) => (
            <Redirect to="/home"/>
          )} />

          <Route path="/home" render={(props) => (
            auth.isAuthenticated() ? (
              <Redirect to="/current"/>
            ) : (
              <Home auth={auth} {...props} />
            )
          )}/>
          <Route path="/profile" render={(props) => (
            !auth.isAuthenticated() ? (
              <Redirect to="/home"/>
            ) : (
              <Profile auth={auth} {...props} />
            )
          )}/>
          <Route path="/current" render={(props) => (
            !auth.isAuthenticated() ? (
              <Redirect to="/home"/>
            ) : (
              <Readings auth={auth} {...props} />
            )
          )}/>
          <Route path="/callback" render={(props) => {
            handleAuthentication(props);
            return <Callback {...props} />
          }}/>
        </div>
      </BrowserRouter>
    </ApolloProvider>
  );
}
